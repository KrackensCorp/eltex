Eltex
небольшое тестовое задание на Junior Frontend вакансию 2020.

Нужно написать программку на любом языке (предпочтительны: с/с++, js, lua, python, java, bash).

1. Программа запрашивает у пользователя число N. Затем генерирует строку из случайных символов длиной N. Случайные символы в данном случае - это любые символы латинского алфавита (A-Z) и цифры (0-9). Сгенерированная строка выводится на экран.
2. Далее программа запрашивает у пользователя один символ. Заменяет все буквы в сгенерированной на шаге 1 строке на этот символ.
3. Запрашивает второй символ. Заменяет все цифры в сгенерированной на шаге 2 строке на второй символ. Выводит получившуюся строку.
4. Выводит количество повторов первого символа и количество повторов второго символа.

Замечание: замену реализовать через цикл for, не через replace или подобные функции.

Реализовано на js